import { somePromise, someAsyncAwait, multipleAwaits, promiseAll } from "../../src/promises/index";

describe("promises", () => {
  const amt = 100;

  const testResolver = resolve => {
    setTimeout(resolve, 1);
  };

  // Its not much, but over a large computation multiple layers of promises can add up.
  describe("plane promise vers promise wrapper in an async await", () => {
    it("runs somePromise", async () => {
      const start = Date.now();
      for (let i = 0; i < amt; i++) {
        await somePromise(testResolver);
      }
      console.log("somePromise end: ", Date.now() - start);
    });

    it("runs someAsyncAwait", async () => {
      const start = Date.now();
      for (let i = 0; i < amt; i++) {
        await someAsyncAwait(testResolver);
      }
      console.log("someAsyncAwait end: ", Date.now() - start);
    });
  });

  // You can get big savings by using Promise all. instead of multiple async awaits. multipleAwaits and promiseAll will return the same thing.
  describe("mulitple awaits vrs Promise.all on the list", () => {
    it("runs multipleAwaits", async () => {
      const start = Date.now();
      for (let i = 0; i < amt; i++) {
        await multipleAwaits(testResolver);
      }
      console.log("multipleAwaits end: ", Date.now() - start);
    });

    it("runs Promise.all", async () => {
      const start = Date.now();
      for (let i = 0; i < amt; i++) {
        await promiseAll(testResolver);
      }
      console.log("promiseAll end: ", Date.now() - start);
    });
  });
});

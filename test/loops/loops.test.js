import { forLoop, forEach } from "../../src/loops/index";

const data = [];
const amt = 10000000;
const testFun = (obj, idx) => {
  const numerator = amt / 10;
  if (idx / numerator === Math.floor(idx / numerator)) {
    console.log("loop: ", idx);
  }
};

// forEach, map, and reduce are great for code simplification, and do really cool things. But if you are handling large arrays, try not to use them. For loops beat them evey time.
describe("loops", () => {
  before(() => {
    // Data initialization for the tests
    for (let i = 0; i < amt; i++) {
      data.push({ id: i + 1, stuff: "junk", foo: "bar" });
    }
  });
  after(() => {
    // Data cleanup
    data.length = 0;
  });

  describe("forEach loop test", () => {
    it("forEach loop", () => {
      const res = forEach(data, testFun);
      console.log("res: ", res);
    });
  });

  describe("for loop test", () => {
    it("forLoop", () => {
      const res = forLoop(data, testFun);
      console.log("res: ", res);
    });
  });
});

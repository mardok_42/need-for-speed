/*
  Method: forEach
  Description: Clean elegat, easy to use. But slower with large numbers
*/
export function forEach(arr, callback) {
  console.log("--- forEach ---");
  const start = Date.now();

  arr.forEach(callback);

  return Date.now() - start;
}

/*
  Method: forLoop
  Description: About 2-3 X the speed of a forEach.
*/
export function forLoop(arr, callback) {
  console.log("--- forLoop ---");
  const start = Date.now();

  for (let i = 0; i < arr.length; i++) {
    callback(arr[i], i);
  }

  return Date.now() - start;
}

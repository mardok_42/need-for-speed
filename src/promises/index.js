/*
  Method: somePromise
  Description: On the whole a plain promise is usually faster than a promise wrapped in an await. But not by much. 
*/
export function somePromise(callback) {
  return new Promise(callback); // Plain Promise
}

/*
  Method: someAsyncAwait
  Description: On the whole a plain promise is usually faster than a promise wrapped in an await. But not by much. 
*/
export async function someAsyncAwait(callback) {
  const res = await new Promise(callback); // Plain Promise wrapped in an await. Essentially this is a promise wrapped in a promise.

  return res;
}

/*
  Method: multipleAwaits
  Description: Yes this is easier to read than a Promise.all, but it is considerably slower. 
*/
export async function multipleAwaits(callback) {
  const res1 = await new Promise(callback);
  const res2 = await new Promise(callback);

  return [res1, res2];
}

/*
  Method: promiseAll
  Description: Much Faster, and gives the same result as multipleAwaits
*/
export async function promiseAll(callback) {
  return Promise.all([new Promise(callback), new Promise(callback)]);
}

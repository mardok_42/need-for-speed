# need-for-speed

### Setup

npm install

### Introduction

There are speed zealots out there, and speed evangelists. I consider myself just religious when it come to optimization and speed. Although I recognize that maintainability and best practices are a good thing. There needs to be balance. These tests are to show that there is is a noticible differance between using common patterns, especially what handling large data sets. Every line does matter, and will slowly decrease the speed of your application. Consider that as you download yet another library that is simply a wrapper for something.

### How to see the results

Run the tests.

npm test

### Run just one test, or a set of tests

Just add an only

it.only - will run just that test
describe.only - will run just that set of tests.
